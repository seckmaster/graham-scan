//package algorithm;
//
//import comparison.Comparator;
//import data.Point;
//import javafx.util.Pair;
//
//import java.util.ArrayList;
//import java.util.List;
//
//public class ConnectPointsSolver {
//  public List<Line> connect(List<Point> points, Algorithm<Point> algorithm, Comparator<Point> comparator) {
//    var result = new ArrayList<Line>(points.size() / 2);
//
//    main_loop:
//    while (!points.isEmpty()) {
//      var convexHull = algorithm.convexHull(points, comparator);
//
//      for (var i = 1; i < convexHull.size() - 1; i++) {
//        var current = convexHull.get(i);
//        var next = convexHull.get(i + 1);
//        if (current.getX() < 0 && next.getX() > 0 || current.getX() > 0 && next.getX() < 0) {
//          result.add(new Line(current, next));
//          points.remove(current);
//          points.remove(next);
//          algorithm.removePoint(current);
//          algorithm.removePoint(next);
//          continue main_loop;
//        }
//      }
//
//      var current = convexHull.get(0);
//      var next = convexHull.get(1);
//      result.add(new Line(current, next));
//      points.remove(current);
//      points.remove(next);
//      algorithm.removePoint(current);
//      algorithm.removePoint(next);
//    }
//    return result;
//  }
//
//  public static class Line extends Pair<Point, Point> {
//    public Line(Point from, Point to) {
//      super(from, to);
//    }
//
//    @Override
//    public String toString() {
//      var builder = new StringBuilder();
//      builder.append(getKey().getId());
//      builder.append("-");
//      builder.append(getValue().getId());
//      return builder.toString();
//    }
//  }
//}

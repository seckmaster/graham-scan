//package algorithm;
//
//import comparison.Comparator;
//import data.Point;
//
//import java.util.*;
//import java.util.stream.Collectors;
//
//public class GrahamScan implements Algorithm<Point> {
//  public static long REMOVED_BASE_COUNT = 0;
//  public static long SORTED_COUNT = 0;
//  public static long CONVEX_HULL_COUNT = 0;
//
//  private Optional<List<Point>> cachedSort = Optional.empty();
//  private Optional<Point> basePoint = Optional.empty();
//  private Optional<Point> leftPoint = Optional.empty();
//  private Optional<Point> rightPoint = Optional.empty();
//
//  public Stack<Point> convexHull(List<Point> points, Comparator comparator) {
//    final var base = findBase(points);
//    final var sortedPoints = sortPoints(points, base, comparator);
//    final var outerPoint = findOuterPoint(base, points);
//
//    long current = System.currentTimeMillis();
//
//    var stack = new Stack<Point>(points.size() + 1);
//
//    for (var point : sortedPoints) {
//      while (stack.size() > 1) {
//        var top = stack.peek();
//        var nextToTop = stack.peekNextToTop();
//        var dir = direction(nextToTop, top, point);
//        if (dir <= 0) break;
//        stack.pop();
//      }
//      stack.push(point);
//
//      if (point.equals(outerPoint)) {
//        CONVEX_HULL_COUNT += System.currentTimeMillis() - current;
//        return stack;
//      }
//    }
//    stack.push(base);
//
//    CONVEX_HULL_COUNT += System.currentTimeMillis() - current;
//
//    return stack;
//  }
//
//  public void removePoint(Point point) {
//    if (!cachedSort.isPresent() || !basePoint.isPresent()) {
//      return;
//    }
//    if (point.equals(basePoint.get())) {
//      cachedSort = Optional.empty();
//      basePoint = Optional.empty();
//      REMOVED_BASE_COUNT++;
//      return;
//    } else if (leftPoint.isPresent() && leftPoint.get().equals(point)) {
//      leftPoint = Optional.empty();
//    } else if (rightPoint.isPresent() && rightPoint.get().equals(point)) {
//      rightPoint = Optional.empty();
//    }
//    cachedSort.get().remove(point);
//  }
//
//  @Override
//  public Point basePoint() {
//    return basePoint.get();
//  }
//
//  private Point findBase(List<Point> points) {
//    if (basePoint.isPresent()) {
//      return basePoint.get();
//    }
//
//    // NOTE: - expecting `points` not to be empty
//    Point base = null;
//    for (Point p : points) {
//      if (base == null) {
//        base = p;
//        continue;
//      }
//
//      if (base.getY() > p.getY()) {
//        base = p;
//      } else if (base.getY() == p.getY() && base.getX() > p.getX()) {
//        base = p;
//      }
//    }
//    basePoint = Optional.of(base);
//    return base;
//  }
//
//  private Point findOuterPoint(Point base, List<Point> points) {
//    return base.getX() < 0 ? findRight(points) : findLeft(points);
//  }
//
//  private Point findLeft(List<Point> points) {
//    if (leftPoint.isPresent()) {
//      return leftPoint.get();
//    }
//
//    // NOTE: - expecting `points` not to be empty
//    Point left = null;
//    for (Point p : points) {
//      if (left == null) {
//        left = p;
//        continue;
//      }
//
//      if (left.getX() > p.getX()) {
//        left = p;
//      } else if (left.getX() == p.getX() && left.getY() > p.getY()) {
//        left = p;
//      }
//    }
//    leftPoint = Optional.of(left);
//    return left;
//  }
//
//  private Point findRight(List<Point> points) {
//    if (rightPoint.isPresent()) {
//      return rightPoint.get();
//    }
//
//    // NOTE: - expecting `points` not to be empty
//    Point right = null;
//    for (Point p : points) {
//      if (right == null) {
//        right = p;
//        continue;
//      }
//
//      if (right.getX() < p.getX()) {
//        right = p;
//      } else if (right.getX() == p.getX() && right.getY() < p.getY()) {
//        right = p;
//      }
//    }
//    rightPoint = Optional.of(right);
//    return right;
//  }
//
//  private List<Point> sortPoints(List<Point> points, Point basePoint, Comparator comparator) {
//    if (cachedSort.isPresent()) {
//      return cachedSort.get();
//    }
//
//    long current = System.currentTimeMillis();
//
////    if (points.size() >= 5000) {
////      var sorted = points
////          .parallelStream()
////          .sorted((p1, p2) -> comparator.compare(p1, p2, basePoint))
////          .collect(Collectors.toList());
////      cachedSort = Optional.of(sorted);
////      SORTED_COUNT += System.currentTimeMillis() - current;
////      return sorted;
////    } else {
//    var sorted = points
//        .stream()
//        .sorted((p1, p2) -> comparator.compare(p1, p2, basePoint))
//        .collect(Collectors.toList());
//    cachedSort = Optional.of(sorted);
//    SORTED_COUNT += System.currentTimeMillis() - current;
//    return sorted;
////    }
//  }
//
//  /**
//   * Relativna smer daljice ij glede na daljico ik
//   * return:
//   * 0 -> Daljici sta vzporedni -> tocke so kolinearne
//   * + -> Daljica ij lezi desno od ik
//   * - -> Daljica ij lezi levo od ik
//   */
//  private double direction(Point i, Point j, Point k) {
//    double x1 = k.getX() - i.getX();
//    double y1 = k.getY() - i.getY();
//    double x2 = j.getX() - i.getX();
//    double y2 = j.getY() - i.getY();
//    return x1 * y2 - x2 * y1;
//  }
//
//  public static class Stack<T> {
//    private Object[] buffer;
//    private int elementsCount = 0;
//
//    public Stack(int capacity) {
//      buffer = new Object[capacity];
//    }
//
//    public T peek() {
//      return (T) buffer[elementsCount - 1];
//    }
//
//    public T peekNextToTop() {
//      return (T) buffer[elementsCount - 2];
//    }
//
//    public void push(T object) {
//      buffer[elementsCount++] = object;
//    }
//
//    public void pop() {
//      --elementsCount;
//    }
//
//    public T get(int index) {
//      return (T) buffer[index];
//    }
//
//    public int size() {
//      return elementsCount;
//    }
//  }
//}

//package algorithm;
//
//import comparison.Comparator;
//import data.PointProtocol;
//
//import java.util.List;
//
//public interface Algorithm<P extends PointProtocol> {
//  GrahamScan.Stack<P> convexHull(List<P> points, Comparator comparator);
//
//  void removePoint(P next);
//
//  P basePoint();
//}

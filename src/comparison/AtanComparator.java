//package comparison;
//
//import data.PointProtocol;
//
//public class AtanComparator<T extends PointProtocol> implements Comparator<T> {
//  public int compare(T a, T b, T base) {
//    double angle1 = atan(a, base);
//    double angle2 = atan(b, base);
//    if (angle1 < angle2) {
//      return -1;
//    }
//    if (angle1 > angle2) {
//      return 1;
//    }
//    return 0;
//  }
//
//  private double atan(T a, T b) {
//    return Math.atan2(
//        a.getY() - b.getY(),
//        a.getX() - b.getX()
//    );
//  }
//}

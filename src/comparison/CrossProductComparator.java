//package comparison;
//
//import data.PointProtocol;
//
//public class CrossProductComparator<T extends PointProtocol> implements Comparator<T> {
//  public int compare(T a, T b, T base) {
//    return crossProductCompare(a, b, base);
//  }
//
//  private int crossProductCompare(T a, T b, T base) {
//    var x1 = a.getX() - base.getX();
//    var y1 = a.getY() - base.getY();
//    var x2 = b.getX() - base.getX();
//    var y2 = b.getY() - base.getY();
//    var surface = x1 * y2 - x2 * y1;
//    if (surface > 0) {
//      return -1;
//    }
//    if (surface < 0) {
//      return 1;
//    }
//    if (a.getY() < b.getY()) {
//      return -1;
//    }
//    if (a.getY() > b.getY()) {
//      return 1;
//    }
//    return 0;
//  }
//}
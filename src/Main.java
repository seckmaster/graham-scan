import javafx.util.Pair;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@SuppressWarnings("unchecked")
public class Main {
  public static class Stack<T> {
    // this implementation of Stack results in a ~2x performance increase compared to java.util.Stack
    // the main reason being that java.util.Stack is based on Vector, instead of ArrayList, resulting in a
    // slow (thread-safe) code

    private Object[] buffer;
    private int elementsCount = 0;

    public Stack(int capacity) {
      buffer = new Object[capacity];
    }

    public T get(int index) {
      return (T) buffer[index];
    }

    public T peek() {
      return get(elementsCount - 1);
    }

    public T peekNextToTop() {
      return get(elementsCount - 2);
    }

    public void push(T object) {
      buffer[elementsCount++] = object;
    }

    public void pop() {
      --elementsCount;
    }

    public int size() {
      return elementsCount;
    }

    public void set(int index, T obj) {
      if (buffer[index] == null) ++elementsCount;
      buffer[index] = obj;
    }

    public Stream<T> stream() {
      return (Stream<T>) Arrays.asList(buffer).stream().filter(o -> o != null);
    }
  }

  public interface Algorithm<P extends PointProtocol> {
    Stack<P> convexHull(List<P> points);

    void removePoint(P next);
  }

  public static long REMOVED_BASE_COUNT = 0;
  public static long SORTED_COUNT = 0;
  public static long CONVEX_HULL_COUNT = 0;

  public static class GrahamScan implements Algorithm<Point> {
    public final Comparator comparator;

    private Optional<List<Point>> cachedSort = Optional.empty();
    private Optional<Point> anchorPoint = Optional.empty();
    private Optional<Point> leftPoint = Optional.empty();
    private Optional<Point> rightPoint = Optional.empty();


    public GrahamScan(Comparator comparator) {
      this.comparator = comparator;
    }

    public Stack<Point> convexHull(List<Point> points) {
      long current = System.currentTimeMillis();

      final var anchor = findAnchor(points);
      final var sortedPoints = sortPoints(points, anchor, comparator);
      final var outerPoint = findOuterPoint(anchor, points);

      var convexHull = new Stack<Point>(points.size() + 1);

      for (var point : sortedPoints) {
        while (convexHull.size() > 1) {
          var top = convexHull.peek();
          var nextToTop = convexHull.peekNextToTop();
          var dir = direction(nextToTop, top, point);
          if (dir <= 0) break;
          convexHull.pop();
        }
        convexHull.push(point);

        if (point.equals(outerPoint)) {
          CONVEX_HULL_COUNT += System.currentTimeMillis() - current;
          return convexHull;
        }
      }
      convexHull.push(anchor);

      CONVEX_HULL_COUNT += System.currentTimeMillis() - current;

      return convexHull;
    }

    public void removePoint(Point point) {
      if (!cachedSort.isPresent() || !anchorPoint.isPresent()) {
        return;
      }
      if (point.equals(anchorPoint.get())) {
        cachedSort = Optional.empty();
        anchorPoint = Optional.empty();
        REMOVED_BASE_COUNT++;
        return;
      } else if (leftPoint.isPresent() && leftPoint.get().equals(point)) {
        leftPoint = Optional.empty();
      } else if (rightPoint.isPresent() && rightPoint.get().equals(point)) {
        rightPoint = Optional.empty();
      }
      cachedSort.get().remove(point);
    }

    private Point findAnchor(List<Point> points) {
      if (anchorPoint.isPresent()) {
        return anchorPoint.get();
      }

      // NOTE: - expecting `points` not to be empty
      Point anchor = null;
      for (Point p : points) {
        if (anchor == null) {
          anchor = p;
          continue;
        }

        if (anchor.getY() > p.getY()) {
          anchor = p;
        } else if (anchor.getY() == p.getY() && anchor.getX() > p.getX()) {
          anchor = p;
        }
      }
      anchorPoint = Optional.of(anchor);
      return anchor;
    }

    private Point findOuterPoint(Point anchor, List<Point> points) {
      return anchor.getX() < 0 ? findRight(points) : findLeft(points);
    }

    private Point findLeft(List<Point> points) {
      if (leftPoint.isPresent()) {
        return leftPoint.get();
      }

      // NOTE: - expecting `points` not to be empty
      Point left = null;
      for (Point p : points) {
        if (left == null) {
          left = p;
          continue;
        }

        if (left.getX() > p.getX()) {
          left = p;
        } else if (left.getX() == p.getX() && left.getY() > p.getY()) {
          left = p;
        }
      }
      leftPoint = Optional.of(left);
      return left;
    }

    private Point findRight(List<Point> points) {
      if (rightPoint.isPresent()) {
        return rightPoint.get();
      }

      // NOTE: - expecting `points` not to be empty
      Point right = null;
      for (Point p : points) {
        if (right == null) {
          right = p;
          continue;
        }

        if (right.getX() < p.getX()) {
          right = p;
        } else if (right.getX() == p.getX() && right.getY() < p.getY()) {
          right = p;
        }
      }
      rightPoint = Optional.of(right);
      return right;
    }

    private List<Point> sortPoints(List<Point> points, Point anchor, Comparator comparator) {
      if (cachedSort.isPresent()) {
        return cachedSort.get();
      }

      long current = System.currentTimeMillis();

//    if (points.size() >= 5000) {
//      var sorted = points
//          .parallelStream()
//          .sorted((p1, p2) -> comparator.compare(p1, p2, anchorPoint))
//          .collect(Collectors.toList());
//      cachedSort = Optional.of(sorted);
//      SORTED_COUNT += System.currentTimeMillis() - current;
//      return sorted;
//    } else {
      var sorted = points
          .stream()
          .sorted((p1, p2) -> comparator.compare(p1, p2, anchor))
          .collect(Collectors.toList());
      cachedSort = Optional.of(sorted);
      SORTED_COUNT += System.currentTimeMillis() - current;
      return sorted;
//    }
    }

    /**
     * Relativna smer daljice ij glede na daljico ik
     * return:
     * 0 -> Daljici sta vzporedni -> tocke so kolinearne
     * + -> Daljica ij lezi desno od ik
     * - -> Daljica ij lezi levo od ik
     */
    private double direction(Point i, Point j, Point k) {
      double x1 = k.getX() - i.getX();
      double y1 = k.getY() - i.getY();
      double x2 = j.getX() - i.getX();
      double y2 = j.getY() - i.getY();
      return x1 * y2 - x2 * y1;
    }

    @Override
    public String toString() {
      return "Graham scan";
    }
  }

  public static class GiftWrapping implements Algorithm<Point> {
    Optional<Point> leftPoint = Optional.empty();
    Optional<Point> rightPoint = Optional.empty();

    @Override
    public Stack convexHull(List<Point> points) {
      long current = System.currentTimeMillis();

      var pointOnHull = findLeft(points);
      var right = findRight(points);
      var convexHull = new Stack<Point>(points.size() + 1);
      Point endpoint;

      do {
        if (pointOnHull.equals(right)) {
          CONVEX_HULL_COUNT += System.currentTimeMillis() - current;
          convexHull.push(pointOnHull);
          return convexHull;
        }

        convexHull.push(pointOnHull);
        endpoint = points.get(0);
        for (var j = 1; j < points.size(); j++) {
          var dir = direction(points.get(j), convexHull.peek(), endpoint);
          if (endpoint.equals(pointOnHull) || dir < 0) {
            endpoint = points.get(j);
          }
        }
        pointOnHull = endpoint;
      } while (!endpoint.equals(convexHull.get(0)));
      convexHull.push(convexHull.get(0));
      CONVEX_HULL_COUNT += System.currentTimeMillis() - current;
      return convexHull;
    }

    @Override
    public void removePoint(Point next) {
      if (leftPoint.isPresent() && leftPoint.get().equals(next)) {
        leftPoint = Optional.empty();
        REMOVED_BASE_COUNT++;
      } else if (rightPoint.isPresent() && rightPoint.get().equals(next)) {
        rightPoint = Optional.empty();
      }
    }

    private Point findLeft(List<Point> points) {
      if (leftPoint.isPresent()) {
        return leftPoint.get();
      }

      // NOTE: - expecting `points` not to be empty
      Point left = null;
      for (Point p : points) {
        if (left == null) {
          left = p;
          continue;
        }

        if (left.getX() > p.getX()) {
          left = p;
        } else if (left.getX() == p.getX() && left.getY() > p.getY()) {
          left = p;
        }
      }
      leftPoint = Optional.of(left);
      return left;
    }

    private Point findRight(List<Point> points) {
      if (rightPoint.isPresent()) {
        return rightPoint.get();
      }

      // NOTE: - expecting `points` not to be empty
      Point right = null;
      for (Point p : points) {
        if (right == null) {
          right = p;
          continue;
        }

        if (right.getX() < p.getX()) {
          right = p;
        } else if (right.getX() == p.getX() && right.getY() < p.getY()) {
          right = p;
        }
      }
      rightPoint = Optional.of(right);
      return right;
    }

    private double direction(Point i, Point j, Point k) {
      double x1 = k.getX() - i.getX();
      double y1 = k.getY() - i.getY();
      double x2 = j.getX() - i.getX();
      double y2 = j.getY() - i.getY();
      return x1 * y2 - x2 * y1;
    }

    @Override
    public String toString() {
      return "Gift wrapping";
    }
  }

  public static class ConnectPointsSolver {
    public List<ConnectPointsSolver.Line> connect(List<Point> points, Algorithm<Point> algorithm) {
      var result = new ArrayList<ConnectPointsSolver.Line>(points.size() / 2);

      main_loop:
      while (!points.isEmpty()) {
        var convexHull = algorithm.convexHull(points);

        for (var i = 1; i < convexHull.size() - 1; i++) {
          var current = convexHull.get(i);
          var next = convexHull.get(i + 1);
          if (current.getX() < 0 && next.getX() > 0 || current.getX() > 0 && next.getX() < 0) {
            result.add(new ConnectPointsSolver.Line(current, next));
            points.remove(current);
            points.remove(next);
            algorithm.removePoint(current);
            algorithm.removePoint(next);
            continue main_loop;
          }
        }

        var current = convexHull.get(0);
        var next = convexHull.get(1);
        result.add(new ConnectPointsSolver.Line(current, next));
        points.remove(current);
        points.remove(next);
        algorithm.removePoint(current);
        algorithm.removePoint(next);
      }
      return result;
    }

    public static class Line extends Pair<Point, Point> {
      public Line(Point from, Point to) {
        super(from, to);
      }

      @Override
      public String toString() {
        var builder = new StringBuilder();
        builder.append(getKey().getId());
        builder.append("-");
        builder.append(getValue().getId());
        return builder.toString();
      }
    }
  }

  public interface Comparator<T extends PointProtocol> {
    int compare(T a, T b, T anchor);
  }

  public static class CrossProductComparator<T extends PointProtocol> implements Comparator<T> {
    public int compare(T a, T b, T anchor) {
      return crossProductCompare(a, b, anchor);
    }

    private int crossProductCompare(T a, T b, T anchor) {
      var x1 = a.getX() - anchor.getX();
      var y1 = a.getY() - anchor.getY();
      var x2 = b.getX() - anchor.getX();
      var y2 = b.getY() - anchor.getY();
      var surface = x1 * y2 - x2 * y1;
      if (surface > 0) {
        return -1;
      }
      if (surface < 0) {
        return 1;
      }
      if (a.getY() < b.getY()) {
        return -1;
      }
      if (a.getY() > b.getY()) {
        return 1;
      }
      return 0;
    }
  }

  public interface PointProtocol {
    double getX();

    double getY();
  }

  public static class Point implements PointProtocol {
    protected final int id;
    protected final double x;
    protected final double y;

    public Point(int id, double x, double y) {
      this.id = id;
      this.x = x;
      this.y = y;
    }

    @Override
    public double getX() {
      return x;
    }

    @Override
    public double getY() {
      return y;
    }

    public int getId() {
      return id;
    }

    @Override
    public String toString() {
      var builder = new StringBuilder();
      builder.append(x);
      builder.append(",");
      builder.append(y);
      return builder.toString();
    }
  }

  public static class Renderer {
    void render(List<Point> points, Stack<Point> convexHull, List<ConnectPointsSolver.Line> lines) throws IOException, InterruptedException {
      var range = points.stream().max(java.util.Comparator.comparingDouble(value -> value.getY())).get();

      var outputStream = new FileOutputStream(".tmp.py");
      var stream = new PrintStream(outputStream);
      stream.println("from matplotlib import pyplot as plt");
      stream.println();
      stream.println("points = [");
      stream.println(points.stream().map(Point::toString).collect(Collectors.joining(",")));
      stream.println("]");
      stream.println("plt.plot(");
      stream.println("[" + convexHull.stream().map(extPoint -> "" + extPoint.getX()).collect(Collectors.joining(",")) + "]");
      stream.println(",");
      stream.println("[" + convexHull.stream().map(extPoint -> "" + extPoint.getY()).collect(Collectors.joining(",")) + "]");
      stream.println(", color=\"red\")");
      stream.println("plt.plot([0,0], [0, " + (range.getY() + 20) + "], color=\"black\", linewidth=1)");
      for (var line : lines) {
        stream.println("plt.plot(");
        stream.println("[" + line.getKey().getX() + ", " + line.getValue().getX() + "], ");
        stream.println("[" + line.getKey().getY() + ", " + line.getValue().getY() + "]");
        stream.println(", color=\"green\", linewidth=0.7)");
      }
      stream.println("plt.scatter(points[::2], points[1::2], s=10)");
      stream.println("plt.show()");

      var rt = Runtime.getRuntime();
      var process = rt.exec("python3 .tmp.py");
      process.waitFor();

      Files.delete(Paths.get(".tmp.py"));
    }
  }

  public static void main(String[] args) throws IOException, InterruptedException {
    var mode = 0;
    if (args.length > 0) {
      try {
        mode = Integer.parseInt(args[0]);
      } catch (Exception e) {
        ///
      }
    }

    switch (mode) {
      case 0:
        submission(args[0]);
        break;
      case 1:
        render(40);
        break;
      case 2:
        benchmark(new GrahamScan(new CrossProductComparator()));
        break;
      case 3:
        renderJarvis(20);
        break;
      case 4:
        benchmark(new GiftWrapping());
        break;
    }
  }

  private static void submission(String fileName) throws IOException, InterruptedException {
    var points = Files.readAllLines(Paths.get(fileName)).stream()
        .skip(1)
        .map(s -> s.split(","))
        .map(strings -> pointFromStringArray(strings))
        .collect(Collectors.toList());
    var copy = new ArrayList<>(points);
    var algorithm = new GrahamScan(new CrossProductComparator<>());
    var result = new ConnectPointsSolver().connect(points, algorithm);
    result.stream()
        .map(ConnectPointsSolver.Line::toString)
        .forEach(s -> System.out.println(s));
    new Renderer().render(copy, new Stack<>(0), result);
  }

  private static Point pointFromStringArray(String[] array) {
    var id = Integer.parseInt(array[0]);
    var x = Double.parseDouble(array[1]);
    var y = Double.parseDouble(array[2]);
    return new Point(id, x, y);
  }

  private static void render(int count) throws IOException, InterruptedException {
    var points = generateData(count, new Random().nextInt(), 10000);
    var copy = new ArrayList<>(points);
    var algorithm = new GrahamScan(new CrossProductComparator());
    var lines = new ConnectPointsSolver().connect(points, algorithm);
    new Renderer().render(copy, new Stack<>(0), lines);
  }

  private static void renderJarvis(int count) throws IOException, InterruptedException {
    var points = generateData(count, new Random().nextInt(), 10000);
    var copy = new ArrayList<>(points);
    var algorithm = new GiftWrapping();
    var convexHull = algorithm.convexHull(points);
    var lines = new ConnectPointsSolver().connect(points, algorithm);
    new Renderer().render(copy, convexHull, lines);
  }

  private static void benchmark(Algorithm algorithm) throws FileNotFoundException {
    var idx = 1;
    try {
      var reader = new BufferedReader(new FileReader("out/.counter"));
      idx = Integer.parseInt(reader.readLine()) + 1;
      reader.close();
    } catch (IOException e) {
    }
    try {
      var writer = new BufferedWriter(new FileWriter("out/.counter"));
      writer.write(String.valueOf(idx));
      writer.close();
    } catch (IOException e) {
    }
    var outputStream = new PrintStream(new FileOutputStream("out/output[" + idx + "].txt"));
    outputStream.print(algorithm.toString() + ":");
    outputStream.println();
    outputStream.println(lastCommitSummary());

    var startTime = System.currentTimeMillis();
    var counts = new int[]{10, 100, 500, 1000, 2000, 5000, 10000, 20000};
    idx = 1;
    for (var count : counts) {
      for (var i = 1; i <= 5; i++) {
        int seed = count ^ i;
        calculateIntersections(idx++, count, seed, algorithm, outputStream);
      }
    }

    outputStream.println("-> Calculated in: " + (System.currentTimeMillis() - startTime) + "ms");
    outputStream.println("Removed base count: " + REMOVED_BASE_COUNT);
    outputStream.println("Sorts [ms]: " + SORTED_COUNT);
    outputStream.println("Hull [ms]: " + CONVEX_HULL_COUNT);
  }

  private static void calculateIntersections(int index, int count, int seed, Algorithm<Point> algorithm, PrintStream stream) {
    var points = generateData(count, seed, 40000);
    var startTime = System.currentTimeMillis();
    var lines = new ConnectPointsSolver().connect(points, algorithm);
    stream.println("" + index + "; Count: " + count + ", seed: " + seed);
    stream.println("-> Calculated in: " + (System.currentTimeMillis() - startTime) + "ms");
    stream.println();
  }

  private static List<Point> generateData(int count, int seed, double range) {
    if (count < 3 && count % 2 == 0) {
      System.out.println("Count should be at least `3` and divisible by 2!");
      System.exit(99);
    }

    var generator = new Random(seed);
    var points = IntStream.range(0, count).mapToObj(i -> {
      double x = generator.nextDouble() * range;
      x = i % 2 == 0 ? x : -x;
      double y = generator.nextDouble() * range;
      return new Point(i, x, y);
    }).collect(Collectors.toList());
    return points;
  }

  private static String lastCommitSummary() {
    try {
      var rt = Runtime.getRuntime();
      var process = rt.exec("git log -1");
      var reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
      String s;
      var builder = new StringBuilder();
      while ((s = reader.readLine()) != null) {
        builder.append(s);
        builder.append("\n");
      }
      builder.append("\n---------\n\n");
      return builder.toString();
    } catch (IOException e) {
      return "";
    }
  }
}

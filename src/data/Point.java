//package data;
//
//public class Point implements PointProtocol {
//  protected final int id;
//  protected final double x;
//  protected final double y;
//
//  public Point(int id, double x, double y) {
//    this.id = id;
//    this.x = x;
//    this.y = y;
//  }
//
//  @Override
//  public double getX() {
//    return x;
//  }
//
//  @Override
//  public double getY() {
//    return y;
//  }
//
//  public int getId() {
//    return id;
//  }
//
//  @Override
//  public String toString() {
//    var builder = new StringBuilder();
//    builder.append(x);
//    builder.append(",");
//    builder.append(y);
//    return builder.toString();
//  }
//}
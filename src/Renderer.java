//import algorithm.ConnectPointsSolver;
//import data.Point;
//
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.io.PrintStream;
//import java.nio.file.Files;
//import java.nio.file.Paths;
//import java.util.Comparator;
//import java.util.List;
//import java.util.stream.Collectors;
//
//public class Renderer {
//  void render(List<Point> points, List<Point> convexHull, List<ConnectPointsSolver.Line> lines) throws IOException, InterruptedException {
//    var range = points.stream().max(Comparator.comparingDouble(value -> value.getY())).get();
//
//    var outputStream = new FileOutputStream(".tmp.py");
//    var stream = new PrintStream(outputStream);
//    stream.println("from matplotlib import pyplot as plt");
//    stream.println();
//    stream.println("points = [");
//    stream.println(points.stream().map(Point::toString).collect(Collectors.joining(",")));
//    stream.println("]");
//    stream.println("plt.plot(");
//    stream.println("[" + convexHull.stream().map(extPoint -> "" + extPoint.getX()).collect(Collectors.joining(",")) + "]");
//    stream.println(",");
//    stream.println("[" + convexHull.stream().map(extPoint -> "" + extPoint.getY()).collect(Collectors.joining(",")) + "]");
//    stream.println(", color=\"red\")");
//    stream.println("plt.plot([0,0], [0, " + (range.getY() + 20) + "], color=\"black\", linewidth=1)");
//    for (var line : lines) {
//      stream.println("plt.plot(");
//      stream.println("[" + line.getKey().getX() + ", " + line.getValue().getX() + "], ");
//      stream.println("[" + line.getKey().getY() + ", " + line.getValue().getY() + "]");
//      stream.println(", color=\"green\", linewidth=0.7)");
//    }
//    stream.println("plt.scatter(points[::2], points[1::2], s=10)");
//    stream.println("plt.show()");
//
//    var rt = Runtime.getRuntime();
//    var process = rt.exec("python3 .tmp.py");
//    process.waitFor();
//
//    Files.delete(Paths.get(".tmp.py"));
//  }
//}
